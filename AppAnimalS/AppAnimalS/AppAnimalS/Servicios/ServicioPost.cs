﻿using AppAnimalS.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppAnimalS.Servicios
{
    public class ServicioPost
    {
        #region Método desarrollado de la manera correcta
        public async Task<Response> GetList<T>(string urlBase, string prefijo, string controlador)
        {
            try
            {
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri(urlBase);
                //concatena dos string
                var url = $"{prefijo}{controlador}";
                var response = await cliente.GetAsync(url);
                var answer = await response.Content.ReadAsStringAsync();

                //Si el servicio falla
                if (!response.IsSuccessStatusCode)
                {
                    //devolvemos nueva respuesta
                    return new Response
                    {
                        IsSuccess = false,
                        Message = answer,
                    };
                }

                //Si llega hasta aca es porque el servicio dio ok
                //Deserealizamos
                var lista = JsonConvert.DeserializeObject<List<T>>(answer);

                //devolvemos respuesta
                return new Response
                {
                    IsSuccess = true,
                    Result = lista,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
        #endregion

        #region Método que hace lo mismo que el de arriba pero anda
        public async Task<Response> GetList<T>(string url)
        {
            try
            {
                var cliente = new HttpClient();
                var response = await cliente.GetAsync(url);
                var answer = await response.Content.ReadAsStringAsync();

                //Si el servicio falla
                if (!response.IsSuccessStatusCode)
                {
                    //devolvemos nueva respuesta
                    return new Response
                    {
                        IsSuccess = false,
                        Message = answer,
                    };
                }

                //Si llega hasta aca es porque el servicio dio ok
                //Deserealizamos
                var lista = JsonConvert.DeserializeObject<List<T>>(answer);

                //devolvemos respuesta
                return new Response
                {
                    IsSuccess = true,
                    Result = lista,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
        #endregion
    }
}
