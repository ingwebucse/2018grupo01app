﻿using AppAnimalS.Servicios;
using AppAnimalS.ViewModels;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppAnimalS.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
            //llama al método que va a carcar la lista de post
            Cargar_Lista_Post();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PostPage());
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new PostPage()));
          
        }

        //
        public ObservableCollection<ListPostResponse> ListaPublicaciones { get; set; }

        //
        private async void Cargar_Lista_Post()
        {
            //Inicializa
            ServicioPost server = new ServicioPost();

            //URL base
            var urlBase = "https://animalitos-s.herokuapp.com/api/v1/post_zona/";

            #region Obtengo ubicación
            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var latitude = position.Latitude.ToString();
            var longitude = position.Longitude.ToString();
            #endregion

            var url = $"{urlBase}{latitude}{"/"}{longitude}";
            var response = await server.GetList<ListPostResponse>(url);

            if (!response.IsSuccess)
            {
                await DisplayAlert("Error", response.Message, "Aceptar");
                return;
            }

            var list = (List<ListPostResponse>)response.Result;

            ListaPublicaciones = new ObservableCollection<ListPostResponse>(list);

            lstPost.ItemsSource = ListaPublicaciones;
        }
    }
}