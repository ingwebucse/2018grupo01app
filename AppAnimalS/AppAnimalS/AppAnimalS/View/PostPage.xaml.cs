﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.Net.Http;
using AppAnimalS.ViewModels;

namespace AppAnimalS.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostPage : ContentPage
	{
        List<Categorias> lstcategria = new List<Categorias>();
        List<TipoAnimal> lstTipoAnimals = new List<TipoAnimal>();

        public PostPage ()
		{
			InitializeComponent ();

            Cargar_Picker();

        }

        private void Cargar_Picker()
        {
            #region Categorias
            lstcategria.Add(new Categorias() { idCategoria = "1", descripCategoria = "Perdido" });
            lstcategria.Add(new Categorias() { idCategoria = "2", descripCategoria = "Peligroso" });
            lstcategria.Add(new Categorias() { idCategoria = "3", descripCategoria = "En adopción" });
            lstcategria.Add(new Categorias() { idCategoria = "4", descripCategoria = "Encontrado" });
            lstcategria.Add(new Categorias() { idCategoria = "5", descripCategoria = "Callejero" });
            

            PckCategorias.ItemsSource = lstcategria;
            #endregion

            #region Tipo de animales
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "1", descripTipoAnimal = "Perro" });
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "2", descripTipoAnimal = "Gato" });
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "3", descripTipoAnimal = "Tortuga" });
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "4", descripTipoAnimal = "Conejo" });
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "5", descripTipoAnimal = "Otro" });
            lstTipoAnimals.Add(new TipoAnimal() { idTipoAnimal = "6", descripTipoAnimal = "Ave" });
            

            PckTipoAnimal.ItemsSource = lstTipoAnimals;
            #endregion

        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            #region Request de prueba para la api
            var request = new PostRequest
            {
                idCategory = lstcategria[PckCategorias.SelectedIndex].idCategoria,
                idAnimalType = lstTipoAnimals[PckTipoAnimal.SelectedIndex].idTipoAnimal,
                nameAnimal = AnimalName.Text,
                description = ComentarioInput.Text,
                ubication = $"{"SRID=4326;POINT ("}{lblLatitude.Text}{" "}{lblLongitude.Text}{")"}",
                image = null,
                idPostType = "1",
                idStatus = "1",
                idUser = null,
            };
            #endregion

            //Parseo
            var Jeison = JsonConvert.SerializeObject(request);

            //inicialización
            HttpContent _post = new StringContent(Jeison);
            //mmm no se
            _post.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            //Respuesta
            await new HttpClient().PostAsync("https://animalitos-s.herokuapp.com/api/v1/create_post", _post);

            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            lblLatitude.Text = position.Latitude.ToString();
            lblLongitude.Text = position.Longitude.ToString();
        }

        public bool IsLocationAvailable()
        {
            return CrossGeolocator.Current.IsGeolocationAvailable;
        }

        //private async void Button_Clicked_ElegirFoto(object sender, EventArgs e)
        //{
        //    if (CrossMedia.Current.IsTakePhotoSupported)
        //    {
        //        var image = await CrossMedia.Current.PickPhotoAsync();
        //        if (image != null)
        //        {
        //            MiImagen.Source = ImageSource.FromStream(() =>
        //            {
        //                var stream = image.GetStream();
        //                image.Dispose();
        //                return stream;
        //            });
        //        }
        //    }
        //}
    }
}