﻿using AppAnimalS.ViewModels;
using Newtonsoft.Json;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppAnimalS.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Task.Run(RequestPermissions);
        }

        private async Task RequestPermissions()
        {
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Microphone);
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtUserName.Text) && !String.IsNullOrEmpty(TxtPassword.Text))
            {
                var request = new LoginRequest
                {
                    username = TxtUserName.Text,
                    email = "",
                    password = TxtPassword.Text
                };

                //Parseo
                var Jeison = JsonConvert.SerializeObject(request);

                //inicialización
                HttpContent _login = new StringContent(Jeison);
                //mmm no se
                _login.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                //Respuesta
                var response = await new HttpClient().PostAsync("https://animalitos-s.herokuapp.com/api/v1/rest-auth/login/", _login);

                //Si autorización dio true por se registró
                if (response.IsSuccessStatusCode)
                {
                    await Navigation.PushAsync(new HomePage());
                }
                else
                {
                    await DisplayAlert("Error", "Usuaro o contraseña incorrectos", "Aceptar");
                }
            }
            else
            {
                await DisplayAlert("Error", "El campo Usuaro y contraseña son obligatorios", "Aceptar");
            };
        }
    }
}