﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppAnimalS.ViewModels
{
    public class Categorias
    {
        public string idCategoria { get; set; }
        public string descripCategoria { get; set; }

        public override string ToString()
        {
            return descripCategoria;
        }
    }
}
