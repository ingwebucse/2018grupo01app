﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppAnimalS.ViewModels
{
    public class LoginRequest
    {
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
