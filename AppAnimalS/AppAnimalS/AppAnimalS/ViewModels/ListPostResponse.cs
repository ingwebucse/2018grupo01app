﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppAnimalS.ViewModels
{
    public class ListPostResponse
    {
        public string id { get; set; }
        public string idCategory { get; set; }
        public string idAnimalType { get; set; }
        public string nameAnimal { get; set; }
        public string description { get; set; }
        public string ubication { get; set; }
        public string image { get; set; }
        public string idPostType { get; set; }
        public string idStatus { get; set; }
        public string idUser { get; set; }
    }

    //Código de prueba para postear una publicación
    public class PostRequest
    {
        public string idCategory { get; set; }
        public string idAnimalType { get; set; }
        public string nameAnimal { get; set; }
        public string description { get; set; }
        public string ubication { get; set; }
        public string image { get; set; }
        public string idPostType { get; set; }
        public string idStatus { get; set; }
        public string idUser { get; set; }
    }
}
