﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppAnimalS.ViewModels
{
    public class TipoAnimal
    {
        public string idTipoAnimal { get; set; }
        public string descripTipoAnimal { get; set; }

        public override string ToString()
        {
            return descripTipoAnimal;
        }
    }
}
