﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppAnimalS.ViewModels
{
    public class Response
    {
        //Si se pudo conectar o no
        public bool IsSuccess { get; set; }
        //Si no se pudo conectar, muestra mensaje de error
        public string Message { get; set; }
        //Si se conectó muestra el resultado
        public object Result { get; set; }
    }
}
